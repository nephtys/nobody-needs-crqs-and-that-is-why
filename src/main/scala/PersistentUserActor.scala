import CommandsAndResults._
import akka.actor.{ActorRef, Props}
import akka.persistence.PersistentActor

/**
  * Created by nephtys on 10/22/16.
  */
class PersistentUserActor(name : String, endOfLine : ActorRef) extends PersistentActor {


  println(s"Beginning UserActor Work $name at path = ${context.self.path}")

  override def receiveRecover: Receive = {
    case m : CreateUserEvent => //endOfLine ! User(m.name)
    case m : MakePurchaseEvent => endOfLine ! m
    case m : Any => println("UserActor recovered: " + m)
  }

  override def receiveCommand: Receive = {
    case m : CreateUserEvent => {
      //(endOfLine ! User(m.name))
    }
    case m : MakePurchaseEvent => {
      persist(m)(a => endOfLine ! m)
    }
    case m : Any => println(s"UserActor $name received: " + m)
  }

  override def persistenceId: String = "user/"+name
}


object PersistentUserActor {
  def props(name : String, endOfLine: ActorRef): Props = Props(new PersistentUserActor(name, endOfLine))
}