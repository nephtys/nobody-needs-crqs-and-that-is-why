import java.util.concurrent.TimeUnit

import CommandsAndResults.{Item, User}
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.util.Timeout

import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.immutable.Stack
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success}

/**
  * Created by nephtys on 10/22/16.
  */
class CommandsAndResults(implicit actorSystem: ActorSystem) {

  private val endOfLine : ActorRef = actorSystem.actorOf(EndOfTheLineActor.props(this))

  private val storeKeeper : ActorRef = actorSystem.actorOf(StoreKeeperActor.props(endOfLine))


  def createUser(name : String) : Unit = {
    val msg = CommandsAndResults.CreateUserEvent(name)
    storeKeeper ! msg
  }
  def createItem(name : String) : Unit = {
    val msg = CommandsAndResults.CreateItemEvent(name)
    storeKeeper ! msg
  }
  def makePurchase(nameOfUser : String, nameOfItem : String) = {
    val msg = CommandsAndResults.MakePurchaseEvent(nameOfUser, nameOfItem)
    storeKeeper ! msg
  }


  //TODO: this could be done without volatile by using some other kind of implicit synchronization
  @volatile
  var users : Set[User] = Set.empty
  @volatile
  var items : Set[Item] = Set.empty
  @volatile
  var purchases : Map[User, Seq[Item]] = Map.empty


}


object CommandsAndResults {
  case class Item(name : String)
  case class User(name : String)


  /**
    * this is a command in CQRS, but we only do event sourcing, so we call this an 'event'
    * @param name
    */
  case class CreateItemEvent(name : String)
  case class CreateUserEvent(name : String)
  case class MakePurchaseEvent(user : String, item : String)




}