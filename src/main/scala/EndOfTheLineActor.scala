import CommandsAndResults.{Item, MakePurchaseEvent, User}
import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef, Props}

/**
  * Created by nephtys on 10/22/16.
  */
class EndOfTheLineActor(commandsAndResults: CommandsAndResults) extends Actor {
  override def receive: Receive = {
    case m : Item => commandsAndResults.items = commandsAndResults.items.+(m)
    case m : User => commandsAndResults.users = commandsAndResults.users.+(m)
    case m : MakePurchaseEvent => {
      println(s"received MakePurchaseEvent $m at EndOfThLine")
      println(s"Searching in ${commandsAndResults.items}")
      println(s"Searching in ${commandsAndResults.users}")
      if (commandsAndResults.items.isEmpty || commandsAndResults.users.isEmpty) {
        println("one of the sets is empty, dealing with this message later and waiting a few milliseconds first...")
        Thread.sleep(10)
        self ! m
      }

      val item : Item = commandsAndResults.items.find(_.name == m.item).get
      val user : User = commandsAndResults.users.find(_.name == m.user).get
      val previousPurchases = commandsAndResults.purchases.getOrElse(user, Seq.empty[Item])
      commandsAndResults.purchases = commandsAndResults.purchases + (user -> previousPurchases.+:(item))
    }
    case _ => ???
  }
}

object EndOfTheLineActor {
  def props(commandsAndResults: CommandsAndResults): Props = Props(new EndOfTheLineActor(commandsAndResults))
}