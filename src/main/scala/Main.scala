import akka.actor.ActorSystem

/**
  * Created by nephtys on 10/22/16.
  */
object Main extends App {
  println("Hello World")

  implicit val system = ActorSystem("mysystem")
  val car = new CommandsAndResults()
  println("Starting")

  car.createItem("beer") //does nothing if already created
  //Thread.sleep(50)
  car.createUser("radi") //does nothing if already created
  //Thread.sleep(50)
  car.makePurchase("radi", "beer") //not idemptpotent, therefore stacks with every run

  Thread.sleep(1000)
  println("Shutting down with following purchases done:")
  car.purchases.foreach(f => {
    println("User : " + f._1)
    f._2.foreach(println)
  })


  system.terminate()
}
