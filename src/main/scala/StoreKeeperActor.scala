import java.util.concurrent.TimeUnit

import CommandsAndResults._
import akka.actor.{ActorRef, Props}
import akka.persistence.PersistentActor

import scala.concurrent.ExecutionContext.Implicits.global
import akka.util.Timeout

import scala.collection.{mutable, _}
import scala.collection.convert.decorateAsScala._
import java.util.concurrent.ConcurrentHashMap

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.{Duration, FiniteDuration}
import scala.util.{Failure, Success, Try}

/**
  * Created by nephtys on 10/22/16.
  */
class StoreKeeperActor(endOfLine: ActorRef) extends PersistentActor {

  private val map: mutable.Map[String, ActorRef] = mutable.Map[String,ActorRef]()

  private var users = Set.empty[String]
  private var items = Set.empty[String]


  override def receiveRecover: Receive = {
    case userevent: CreateUserEvent => {
      {
        println(s"keeper recovering $userevent")
        if (!users.contains(userevent.name)) {
          println(s"Storekeeper recovers $userevent")
          val actorname = "user_" + userevent.name
          sendToExistingActorOrCreateNew(actorname, () => context.actorOf(PersistentUserActor.props(actorname, endOfLine),
            actorname))(userevent)
          endOfLine ! User(userevent.name)
          users = users + userevent.name
        }
      }
    }
    case itemevent: CreateItemEvent => {
      {
        println(s"keeper recovering $itemevent")
        if (!items.contains(itemevent.name)) {
          println(s"Storekeeper recovers $itemevent")
          val actorname = "item_" + itemevent.name
          sendToExistingActorOrCreateNew(actorname, () => context.actorOf(PersistentUserActor.props(actorname, endOfLine),
            actorname))(itemevent)
          endOfLine ! Item(itemevent.name)
          items = items + itemevent.name
        }
      }
    }
  }

  override def receiveCommand: Receive = {
    case userevent: CreateUserEvent => {
      println(s"keeper receiving $userevent")
      if (!users.contains(userevent.name)) {
        persist(userevent)(a => {
          println(s"Storekeeper receives $userevent")
          val actorname = "user_" + userevent.name
          sendToExistingActorOrCreateNew(actorname, () => context.actorOf(PersistentUserActor.props(actorname, endOfLine),
            actorname))(userevent)
          endOfLine ! User(userevent.name)
          users = users + userevent.name
        })
      }
    }
    case makeevent : MakePurchaseEvent => {
      println(s"keeper receiving $makeevent")
      val actorname = "user_" + makeevent.user
      sendToExistingActorOrCreateNew(actorname, () => context.actorOf(PersistentUserActor.props(actorname, endOfLine),
        actorname))(makeevent)

    }
    case itemevent: CreateItemEvent => {
      println(s"keeper receiving $itemevent")
      if (!items.contains(itemevent.name)) {
      persist(itemevent)(a => {
        println(s"Storekeeper receives $itemevent")
        val actorname = "item_" + itemevent.name
        sendToExistingActorOrCreateNew(actorname, () => context.actorOf(PersistentUserActor.props(actorname, endOfLine),
          actorname))(itemevent)
        endOfLine ! Item(itemevent.name)
        items = items + itemevent.name
      })
    }
    }

  }

  override def persistenceId: String = "storekeeper"


  /**
    * akka is AWESOME
    *
    * @return actorref for actor with given name
    */
  private def actorResolver(nameOfActor: String): Future[ActorRef] = {
    implicit val timeout = Timeout(FiniteDuration(1, TimeUnit.SECONDS))
    println(s"Searching for $nameOfActor at path = "+ "user/$b/" + nameOfActor)
    //val x =  actorSystem.actorSelection("*" +nameOfActor+"*" )
    //Await.result(x.resolveOne(), FiniteDuration(1, TimeUnit.SECONDS))
    context.actorSelection("user/$b/" + nameOfActor).resolveOne()
  }

  /**
    * simple helper function
    *
    * @param nameOfActor
    * @param message
    */
  private def sendToExistingActorOrCreateNew(nameOfActor: String, createNew: () => ActorRef)(message: Any):
  Unit
  = {
    val r = map.get(nameOfActor)
    if (r.isDefined) {
        r.get ! message
    } else {
      Try(Await.result(actorResolver(nameOfActor), Duration(1, "second"))) match {
        case Success(actorRef) => {
          map.put(nameOfActor, actorRef)
          actorRef ! message
        }
        case Failure(ex) => {
          val x = createNew.apply()
          map.put(nameOfActor, x)
          x  ! message
        }
      }
      //TODO: replace with become(busy)

    }
  }
}


object StoreKeeperActor {
  def props(endOfLine: ActorRef): Props = Props(new StoreKeeperActor(endOfLine))
}