import CommandsAndResults.{CreateItemEvent, Item}
import akka.actor.{ActorRef, Props}
import akka.persistence.PersistentActor

/**
  * name should be a unique id
  * Created by nephtys on 10/22/16.
  */
class PersistentItemActor(name : String, endOfLine : ActorRef) extends PersistentActor {



  println(s"Beginning ItemActor Work at path ${context.self.path}")

  override def receiveRecover: Receive = {
    case m : CreateItemEvent => //endOfLine ! Item(m.name)
    case m : Any => {
      println("ItemActor recovered: " + m)
    }
  }

  override def receiveCommand: Receive = {
    case m : CreateItemEvent => {
      //(endOfLine ! Item(m.name))
    }
    case m : Any => println("ItemActor received: " + m)
  }

  override def persistenceId: String = "item/"+name
}

object PersistentItemActor {
  def props(name : String, endOfLine: ActorRef): Props = Props(new PersistentItemActor(name, endOfLine))
}